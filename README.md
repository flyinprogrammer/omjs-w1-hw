# omjs-w1-hw

Homework from the first week of [this class](https://onemonth.com/courses/javascript).

# Homework

1. On the Oprah app, right now if "Chris" comes to the site he gets a car. Write the code so that if "Chris" and YOU come to the site we both get cars. But no on else gets cars! Just you and me. (hint: else if)

2. Show images for 3 cars in addition to the car names

3. On the Happy Hour Randomizer App concatenate a Google Map embed so that when your bar name comes up the Google Map searches for it and displays the location.

4. Make your own randomizer app!

5. Suggested reading: http://eloquentjavascript.net/01_values.html


# Solutions

1 & 2 are done in [oprah-app](https://gitlab.com/flyinprogrammer/omjs-w1-hw/tree/master/oprah-app)

3 is done in [happyhour-simple](https://gitlab.com/flyinprogrammer/omjs-w1-hw/tree/master/happyhour-simple)

4 is done in [next-programming-language](https://gitlab.com/flyinprogrammer/omjs-w1-hw/tree/master/next-programming-language)